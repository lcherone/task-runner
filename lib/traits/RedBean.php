<?php
namespace Tasks\Lib\Traits;

use RedBeanPHP\R;

trait RedBean {

    /**
     * 
     */
    public function redbeanConnect()
    {
        R::setup(
            $this->task->config['redbean']['dsn'],
            $this->task->config['redbean']['username'],
            $this->task->config['redbean']['password']
        );
        
        R::freeze(($this->task->config['redbean']['freeze'] === true));
        R::debug(($this->task->config['redbean']['debug'] === true));
        
        $this->task->state['redbeanConnected'] = true;
    }

}

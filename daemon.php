<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require 'config.php';
require 'vendor/autoload.php';
require 'lib/Runner.php';

$task = new Tasks\Runner($config);
$task->climate = new League\CLImate\CLImate;

$task->daemon('HelloWorld', ['sleep_time' => 1]);

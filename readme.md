## PHP Tasks Runner

**Author:** Lawrence Cherone

For running tasks at specific times or continuously at n second intervals.

**Tasks:**

A task is a PHP class with 1 method `execute()`. See `./tasks/HelloWorld.php`.


**Powered by cron:**

`* * * * * cd /home/me/tasks && /usr/bin/php /home/me/tasks/cron.php >/dev/null 2>&1`

`* * * * * cd /home/me/tasks && /usr/bin/php /home/me/tasks/daemon.php >/dev/null 2>&1`


### cron.php

    lcherone:~/workspace (master) $ php cron.php 
     _______        _    _____
    |__   __|      | |  |  __ \
       | | __ _ ___| | _| |__) |   _ _ __  _ __   ___ _ __
       | |/ _` / __| |/ /  _  / | | | '_ \| '_ \ / _ \ '__|
       | | (_| \__ \   <| | \ \ |_| | | | | | | |  __/ |
       |_|\__,_|___/_|\_\_|  \_\__,_|_| |_|_| |_|\___|_|   v0.0.1
    
    DEBUG MODE ENABLED:
     - Turn off debug in production, which will stop this output to the console.
    Hello..World!

### daemon.php 

     _______        _    _____
    |__   __|      | |  |  __ \
       | | __ _ ___| | _| |__) |   _ _ __  _ __   ___ _ __
       | |/ _` / __| |/ /  _  / | | | '_ \| '_ \ / _ \ '__|
       | | (_| \__ \   <| | \ \ |_| | | | | | | |  __/ |
       |_|\__,_|___/_|\_\_|  \_\__,_|_| |_|_| |_|\___|_|   v0.0.1
    
    DEBUG MODE ENABLED:
     - Turn off debug in production, which will stop this output to the console.
     - Initial Memory usage: 1.25 megabytes.
     - Sleep time: 1 second between iterations.
     - Stop Time: 22:51:24
    ------------------------------------------------------------------------------------
     # Start Iteration 1
    
    Hello..World!
     - Finished Iteration
     - Took: 2.0160889625549 seconds
     - Memory usage: 1.25 megabytes
     - Sleeping for 1 seconds
     - Total running time 2.0445370674133 seconds
    ------------------------------------------------------------------------------------
     # Start Iteration 2
    
    Hello..World!
     - Finished Iteration
     - Took: 2.0173931121826 seconds
     - Memory usage: 1.25 megabytes
     - Sleeping for 1 seconds
     - Total running time 5.067547082901 seconds
    ------------------------------------------------------------------------------------
    
    \snip..

### tool.php 


    lcherone:~/workspace (master) $ php tool.php 
     _______        _    _____
    |__   __|      | |  |  __ \
       | | __ _ ___| | _| |__) |   _ _ __  _ __   ___ _ __
       | |/ _` / __| |/ /  _  / | | | '_ \| '_ \ / _ \ '__|
       | | (_| \__ \   <| | \ \ |_| | | | | | | |  __/ |
       |_|\__,_|___/_|\_\_|  \_\__,_|_| |_|_| |_|\___|_|   v0.0.1
    
    Please choose from one of the following: (use <space> to select)
    ❯ ● Run Hello World! example.                                                       
     _______        _    _____
    |__   __|      | |  |  __ \
       | | __ _ ___| | _| |__) |   _ _ __  _ __   ___ _ __
       | |/ _` / __| |/ /  _  / | | | '_ \| '_ \ / _ \ '__|
       | | (_| \__ \   <| | \ \ |_| | | | | | | |  __/ |
       |_|\__,_|___/_|\_\_|  \_\__,_|_| |_|_| |_|\___|_|   v0.0.1
    
    DEBUG MODE ENABLED:
     - Turn off debug in production, which will stop this output to the console.
    Hello..World!





<?php
/**
 * Config
 * 
 * Access from within the task like:
 * 
 * $this->task->config['debug']
 * 
 */
 
$config = [
    // displays output to console
    "debug" => true,
    
    // database connection
    "redbean" => [
        "dsn" => "mysql:host=127.0.0.1;dbname=",
        "username" => "root",
        "password" => "",
        "freeze" => false,
        "debug" => false,
    ]
];

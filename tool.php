<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require 'config.php';
require 'vendor/autoload.php';
require 'lib/Runner.php';

$task = new Tasks\Runner($config);
$task->climate = new League\CLImate\CLImate;

$config = [
    'tools' => [
        'HelloWorld' => 'Run Hello World! example.',
        //'...' => '...',
    ]
];

/**
 * Header
 */
$task->climate->addArt('lib/art');
$task->climate->draw('header');

/**
 * Prompt user for tool selection
 */
$input = $task->climate->radio(
    'Please choose from one of the following:', 
    array_values($config['tools'])
);

$tool = array_search($input->prompt(), $config['tools']);

if (!empty($tool)) {
    $task->run($tool);
} else {
    $task->climate->out('<bold><red>Bye!</red></bold>');
}

<?php
namespace Tasks\Task {

    use Tasks\Lib;

    /**
     *
     */
    class HelloWorld
    {
        use Lib\Traits\Log;

        /**
         *
         */
        public function __construct(\Tasks\Runner $task)
        {
            $this->task = $task;
        }

        /**
         *
         */
        public function execute()
        {
            $this->task->climate->inline('Hello');

            for ($i = 0; $i < 2; $i++) {
                $this->task->climate->inline('.');
                sleep(1);
            }

            $this->task->climate->inline('World!');

        }
    }

}

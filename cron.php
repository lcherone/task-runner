<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require 'config.php';
require 'vendor/autoload.php';
require 'lib/Runner.php';

$task = new Tasks\Runner($config);
$task->climate = new League\CLImate\CLImate;

// Set timing variables
$now = [
    'hour'    => (int) date('H'), //0 through 23
    'minute'  => (int) date('i'), //0 to 59
    'weekday' => (int) date('w'), //0 (for Sunday) through 6 (for Saturday)
];

// Tasks that run at 6am
if ($now['hour'] == 6 && $now['minute'] == 0) {
    $task->log('Executing tasks that run at 06.00am', 'info');
    #
}

// Tasks that run at 11.30pm
if ($now['hour'] == 23 && $now['minute'] == 30) {
    $task->log('Executing tasks that run at 11.30pm', 'info');
    #
}

// Tasks that run at 11.59pm
if ($now['hour'] == 23 && $now['minute'] == 59) {
    $task->log('Executing tasks that run a minute before midnight', 'info');
    #
    $task->run('RotateLogs');
}

// Tasks that run at midnight
if ($now['minute'] == 0 && $now['hour'] == 0) {
    $task->log('Executing tasks that run at midnight', 'info');
    #
}

// Tasks that run at midnight Sunday
if ($now['minute'] == 0 && $now['hour'] == 0 && $now['weekday'] == 0) {
    $task->log('Executing tasks that run at midnight Sunday', 'info');
    #
}

// Tasks that run at midday and midnight
if ($now['minute'] == 0 && ($now['hour'] == 0 || $now['hour'] == 12)) {
    $task->log('Executing tasks that run at midday and midnight', 'info');
    #
}

// Tasks that run hourly
if ($now['minute'] == 0) {
    $task->log('Executing tasks that run hourly', 'info');
    #
}

// Tasks that run every 5 mins
if ($now['minute'] %5 == 0) {
    $task->log('Executing tasks that run every 5 mins', 'info');
    #
    $task->run('SetFilePermissions');
}

// Tasks that run every 30 mins
if ($now['minute'] %30 == 0) {
    $task->log('Executing tasks that run every 30 mins', 'info');
    #
}

// Tasks that run every 2 mins
if ($now['minute'] %2 == 0) {
    $task->log('Executing tasks that run every 2 mins', 'info');
}

// Tasks that run every min
$task->log('Executing tasks that run every minute', 'info');
#
$task->run('HelloWorld');
